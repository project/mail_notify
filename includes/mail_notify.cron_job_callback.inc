<?php

/**
 * @file
 * Callback functions for cron job.
 */

use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;

/**
 * The callback for the cron job.
 */
function mail_notify_cron_job_callback($job) {
  $nids = \Drupal::state()->get('mail_notify_updated_nodes');

  if (empty($nids)) {
    return;
  }

  $config = \Drupal::config('mail_notify.settings');
  $roles = $config->get('roles');

  if (empty($roles)) {
    return;
  }

  $recipients_emails = mail_notify_get_recipients_emails($roles);

  if (empty($recipients_emails)) {
    return;
  }

  $from = $config->get('email_from');
  $subject = t($config->get('email_subject'));

  $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($nids);

  $renderer = [
    '#theme' => 'email_body',
    '#nodes' => $nodes,
  ];
  $body = \Drupal::service('renderer')->render($renderer);

  // Add an email to the mail queue.
  foreach ($recipients_emails as $to) {
    simple_mail_queue($from, $to, $subject, $body);
  }
  // Purge the updated nodes list.
  \Drupal::state()->set('mail_notify_updated_nodes', []);
  // Run Cron to start processing the mail queue.
  \Drupal::service('cron')->run();
}

/**
 * Get recipient's emails.
 */
function mail_notify_get_recipients_emails(array $roles) {
  $emails = [];
  $query = \Drupal::entityTypeManager()->getStorage('user')->getQuery();

  if (in_array('authenticated', $roles)) {
    $uids = $query
      ->condition('status', '1')
      ->exists('uid')
      ->execute();
    $users = User::loadMultiple($uids);

    foreach ($users as $user) {
      if ($email = $user->getEmail()) {
        $emails[] = $email;
      }
    }
    return $emails;
  }

  // If the 'authenticated' role was not checked in the settings.
  $uids = $query
    ->condition('status', '1')
    ->condition('roles', $roles, 'IN')
    ->execute();
  $users = User::loadMultiple($uids);

  foreach ($users as $user) {
    if ($email = $user->getEmail()) {
      if (!in_array($email, $emails)) {
        $emails[] = $email;
      }
    }
  }

  return $emails;
}

/**
 * If the 'Pathauto' module is installed, then update a node alias.
 *
 * If we do not do this, then the node alias may not be generated yet.
 * In this case a node's path without an alias will be sent.
 * Also, can be the situation that will be send old, but not updated alias.
 */
function mail_notify_update_node_alias(NodeInterface $node) {
  $module_handler = \Drupal::service('module_handler');

  // Check if the "Pathauto" module is installed.
  if ($module_handler->moduleExists('pathauto')) {
    \Drupal::service('pathauto.generator')->updateEntityAlias($node, 'update');
  }
}

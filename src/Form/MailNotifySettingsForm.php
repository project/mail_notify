<?php

namespace Drupal\mail_notify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The settings form for the 'Mail notify' module.
 */
class MailNotifySettingsForm extends ConfigFormBase {

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Content types.
   *
   * @var array
   */
  protected $contentTypes;

  /**
   * Roles that will receive notifications by mail.
   *
   * @var array
   */
  protected $roles;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->config = $instance->config('mail_notify.settings');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->contentTypes = $instance->entityTypeManager
      ->getStorage('node_type')
      ->loadMultiple();

    $instance->roles = $instance->config->get('roles');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'mail_notify.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mail_notify_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // ----------------- Content types ----------------------------------------.
    $form['content_types_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Content types for which mail notifications will be sent'),
      '#open' => TRUE,
    ];

    $options = [];

    foreach ($this->contentTypes as $content_type) {
      $type = $content_type->id();
      $type_label = $content_type->label();
      $options[$type] = $type_label;
    }

    $form['content_types_details']['content_types'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->config->get('content_types'),
    ];

    // ----------------- Roles ------------------------------------------------.
    $form['roles_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Roles of users who will receive mail notifications'),
      '#open' => TRUE,
    ];

    $options = [];

    foreach (user_roles(TRUE) as $role) {
      $options[$role->id()] = $role->label();
    }

    $form['roles_details']['roles'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $this->config->get('roles'),
    ];

    // ----------------- Email ------------------------------------------------.
    $form['email_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Mail settings'),
      '#open' => TRUE,
    ];

    $site_email = $this->config('system.site')->get('mail');
    $email = $this->config->get('email_from');

    if (empty($email)) {
      $email = $site_email;
    }

    $form['email_details']['email_from'] = [
      '#type' => 'email',
      '#title' => $this->t("Sender's email"),
      '#default_value' => $email,
      '#required' => TRUE,
    ];

    $form['email_details']['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#default_value' => $this->config->get('email_subject'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected_content_types = [];

    foreach ($form_state->getValue('content_types') as $key => $value) {
      if (!empty($value)) {
        $selected_content_types[] = $key;
      }
    }

    $this->config->set('content_types', $selected_content_types);

    $selected_roles = [];

    foreach ($form_state->getValue('roles') as $key => $value) {
      if (!empty($value)) {
        $selected_roles[] = $key;
      }
    }

    $this->config->set('roles', $selected_roles);
    $this->config->set('email_from', $form_state->getValue('email_from'));
    $this->config->set('email_subject', $form_state->getValue('email_subject'));

    $this->config->save();
    parent::submitForm($form, $form_state);
  }

}
